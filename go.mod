module gitlab.codelutin.com/codelutin/web2sms

go 1.14

require (
	github.com/ovh/go-ovh v1.1.0
	github.com/prometheus/alertmanager v0.24.0
)
