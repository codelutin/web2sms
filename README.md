Allow send sms message throw simple http GET/POST

All configuration is done via env variable:
- HOST_PORT (default ":8081") interface and port where server must listen
- OVH_ENDPOINT=ovh-eu
- OVH_APPLICATION_KEY
- OVH_APPLICATION_SECRET
- OVH_CONSUMER_KEY
- OVH_SMS_SERVICE_NAME service name use in sms url jobs
- SMS_SENDER_* mapping between user and valid sender (ex: SMS_SENDER_toto='TOTO')
- SMS_RECEIVER_* mapping between user or group and phone number (ex: SMS_RECEIVER_admsys='+336123445678,+33623456789')

Message is query parameter 'msg'

call pattern:

  curl http://localhost:8081/msg/<SENDER>/<RECEIVER>?msg=<MESSAGE>&status=<firing|resolved>&name=<TYPE>

call example:
```
  curl http://localhost:8081/msg/cl/admsys?msg=Hello
```

How to test localy during development
```
  docker build -t registry.nuiton.org/codelutin/web2sms .
  docker run -it -p8081:8081 --env-file env registry.nuiton.org/codelutin/web2sms
```

Supported client
* simple http GET
* alertmanager POST alert

Supported provider
* ovh
