package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/prometheus/alertmanager/template"
)

func formatAlertmanagerMessage(alert template.Alert) string {
	return fmt.Sprintf("%v - %v\n%v\n\n%v", alert.Status, alert.Labels["severity"], alert.Annotations["summary"], alert.Annotations["description"])
}

func handleAlertmanagerRequest(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	payload := template.Data{}
	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	logger.Printf("Received valid hook from %v", r.RemoteAddr)

	path := strings.Split(r.URL.Path, "/")
	sender, receiver, err := getSenderAndReceiver(path)
	if err != nil {
		http.Error(w, fmt.Sprintf("Could not extract sender and receiver information from path (%v): %v", path, err), 400)
		return
	}

	for _, alert := range payload.Alerts {
		msg := formatAlertmanagerMessage(alert)
		err = smsClient.sendSMS(sender, receiver, msg)
		if err != nil {
			logger.Printf(">> Could not forward to sms: %v", err)
			http.Error(w, "Could not forward to sms.", 500)
			return
		}
	}

	w.WriteHeader(http.StatusOK)
}
