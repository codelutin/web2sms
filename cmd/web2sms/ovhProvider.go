package main

import (
	"fmt"

	"os"

	"github.com/ovh/go-ovh/ovh"
)

type ovhProvider struct {
	url    string
	client *ovh.Client
}

// SMSParams parametre de la requete POST ovh
type SMSParams struct {
	Sender            string   `json:"sender"`
	Receivers         []string `json:"receivers"`
	Message           string   `json:"message"`
	NoStopClause      bool     `json:"noStopClause"`
	SenderForResponse bool     `json:"senderForResponse"`
}

// Response format for a POST request on /sms/{serviceName}/jobs
type jobsResponse struct {
	InvalidReceivers     []string `json:"invalidReceivers"`
	Ids                  []int `json:"ids"`
	TotalCreditsRemoved  int `json:"totalCreditsRemoved"`
	ValidReceivers       []string `json:"validReceivers"`
}

// all informations are read from:
// OVH_ENDPOINT, OVH_APPLICATION_KEY, OVH_APPLICATION_SECRET and OVH_CONSUMER_KEY
// OVH_SMS_SERVICE_NAME
func initOVH() *ovhProvider {
	serviceName := os.Getenv("OVH_SMS_SERVICE_NAME")
	client, err := ovh.NewDefaultClient()

	if err != nil || isEmpty(serviceName) {
		logger.Printf("ovh configuration not found (%q)", err)
		return nil
	}

	return &ovhProvider{url: fmt.Sprintf("/sms/%v/jobs", serviceName), client: client}
}

func (provider *ovhProvider) sendSMS(sender string, receiver []string, msg string) error {
	logger.Printf("%v > %v: %v\n", sender, receiver, msg)

	params := SMSParams{
		Sender:            sender,
		Receivers:         receiver,
		Message:           msg,
		NoStopClause:      true,
		SenderForResponse: false}
	res := jobsResponse{}

	err := provider.client.Post(provider.url, &params, &res)
	if err != nil {
		logger.Println("Error during POST request: ", err)
		return err
	}

	if len(res.InvalidReceivers) > 0 {
		return fmt.Errorf("Some receivers are invalid: %v", res.InvalidReceivers)
	}

	return nil
}
