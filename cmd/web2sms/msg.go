package main

import (
	"fmt"
	"net/http"
	"strings"
)

func formatSimpleMessage(status string, name string, msg string) string {
	return fmt.Sprintf("%v - %v\n\n%v", status, name, msg)
}

func handleMsgRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		status := r.FormValue("status")
		name := r.FormValue("name")
		msg := r.FormValue("msg")

		if isEmpty(msg) {
			http.Error(w, "Missing msg parameter", 400)
			return
		}

		path := strings.Split(r.URL.Path, "/")
		sender, receiver, err := getSenderAndReceiver(path)
		if err != nil {
			http.Error(w, fmt.Sprintf("Could not extract sender and receiver information from path (%v): %v", path, err), 400)
			return
		}

		err = smsClient.sendSMS(sender, receiver, formatSimpleMessage(status, name, msg))
		if err != nil {
			logger.Printf(">> Could not forward to sms: %v", err)
			http.Error(w, "Could not forward to sms.", 500)
		}

	} else {
		http.Error(w, "Invalid request method.", 405)
	}
}
