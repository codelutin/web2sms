package main

// HOST_PORT (default ":8081") interface and port where server must listen
// MATRIX_USER_* token for user *
// MATRIX_ROOM_* id for room *
// OVH_ENDPOINT, OVH_APPLICATION_KEY, OVH_APPLICATION_SECRET and OVH_CONSUMER_KEY

import (
	"log"
	"net/http"
	"os"
)

var logger *log.Logger

var smsClient smsProvider

type smsProvider interface {
	sendSMS(sender string, receiver []string, msg string) error
}

func main() {
	// Initialize logger.
	logger = log.New(os.Stdout, "", log.Flags())

	smsClient = initOVH()

	if smsClient == nil {
		logger.Fatalln("Stop because can't find sms provider")
	}

	// Initialize HTTP server.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	})
	http.Handle("/msg/", http.StripPrefix("/msg/", http.HandlerFunc(handleMsgRequest)))
	http.Handle("/alertmanager/", http.StripPrefix("/alertmanager/", http.HandlerFunc(handleAlertmanagerRequest)))

	port := os.Getenv("HOST_PORT")
	if isEmpty(port) {
		port = ":8081"
	}

	logger.Printf("Listening for HTTP requests (webhooks) on %v", port)
	logger.Fatal(http.ListenAndServe(port, nil))
}
