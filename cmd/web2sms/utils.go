package main

import (
	"fmt"
	"os"
	"strings"
)

func isEmpty(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}

func getSenderAndReceiver(path []string) (string, []string, error) {
	if len(path) != 2 {
		return "", nil, fmt.Errorf("Bad parameter number waiting 2 and got '%v'", len(path))
	}

	senderVar := fmt.Sprintf("SMS_SENDER_%s", path[0])
	sender := os.Getenv(senderVar)
	if isEmpty(sender) {
		return "", nil, fmt.Errorf("Sender not found for '%s'", senderVar)
	}

	receiverVar := fmt.Sprintf("SMS_RECEIVER_%s", path[1])
	receiver := os.Getenv(receiverVar)
	if isEmpty(receiver) {
		return "", nil, fmt.Errorf("Receiver id not found for '%s'", receiverVar)
	}

	receiverList := strings.Split(receiver, ",")

	return sender, receiverList, nil
}
